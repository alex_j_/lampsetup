FROM ubuntu:trusty
MAINTAINER Fernando Mayo <fernando@tutum.co>, Feng Honglin <hfeng@tutum.co>

# Install packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
  apt-get -y install supervisor git apache2 libapache2-mod-php5 mysql-server php5-mysql pwgen php-apc php5-mcrypt && \
  echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Download files
RUN git clone https://github.com/fermayo/hello-world-lamp.git /files
RUN mkdir -p /files/app && rm -fr /var/www/html && ln -s /files/app /var/www/html

# Add image configuration and scripts
RUN mv /files/conf/start-apache2.sh /start-apache2.sh
RUN mv /files/conf/start-mysqld.sh /start-mysqld.sh
RUN mv /files/conf/run.sh /run.sh
RUN chmod 755 /*.sh
RUN mv /files/conf/my.cnf /etc/mysql/conf.d/my.cnf
RUN mv /files/conf/supervisord-apache2.conf /etc/supervisor/conf.d/supervisord-apache2.conf
RUN mv /files/conf/supervisord-mysqld.conf /etc/supervisor/conf.d/supervisord-mysqld.conf

# Remove pre-installed database
RUN rm -rf /var/lib/mysql/*

# Add MySQL utils
RUN mv /files/conf/create_mysql_admin_user.sh /create_mysql_admin_user.sh
RUN chmod 755 /*.sh

# config to enable .htaccess
RUN mv /files/conf/apache_default /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite

#Enviornment variables to configure php
ENV PHP_UPLOAD_MAX_FILESIZE 10M
ENV PHP_POST_MAX_SIZE 10M

# Add volumes for MySQL 
VOLUME  ["/etc/mysql", "/var/lib/mysql" ]

EXPOSE 80 3306
CMD ["/run.sh"]
